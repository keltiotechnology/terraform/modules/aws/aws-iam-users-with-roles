resource "aws_iam_group" "default" {
  for_each = { for group in var.groups : group.name => group }
  name     = each.value.name
}

# IAM user should not be granted permission directly
# By default, we set no permissions to IAM users, but to IAM groups
resource "aws_iam_group_policy" "default" {
  for_each = { for group in var.groups : group.name => group }

  group  = each.value.name
  policy = each.value.policy

  depends_on = [
    aws_iam_group.default
  ]
}

resource "aws_iam_user" "default" {
  for_each = { for user in var.users : user.name => user }
  name     = each.value.name
  tags     = var.iam_user_tags
}

resource "aws_iam_user_group_membership" "default" {
  for_each = { for user in var.users : user.name => user }
  user     = each.value.name
  groups   = each.value.groups

  depends_on = [
    aws_iam_user.default,
    aws_iam_group.default
  ]
}

resource "aws_iam_access_key" "default" {
  for_each = { for user in var.users : user.name => user if user.permissions.create_access_key }
  user     = each.value.name
}

resource "aws_iam_user_login_profile" "default" {
  for_each = { for user in var.users : user.name => user if user.permissions.create_login_profile }
  user     = each.value.name
  pgp_key  = each.value.pgp_key
}

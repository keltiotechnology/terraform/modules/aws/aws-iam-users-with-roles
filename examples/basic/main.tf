module "dev_iam_user" {
  source = "../.."

  groups = [{
    name = "test-admin",
    policy = jsonencode({
      "Version" : "2012-10-17"
      "Statement" : [
        {
          "Action" : [
            "ec2:Describe*",
          ],
          "Effect" : "Allow"
          "Resource" : "*"
        },
      ]
    })
    }
  ]

  users = [{
    name    = "john",
    pgp_key = "keybase:phatvo",
    groups  = ["test-admin"],
    permissions = {
      create_access_key    = true,
      create_login_profile = true
    }
    }, {
    name    = "alex",
    pgp_key = "keybase:phatvo",
    groups  = ["test-admin"],
    permissions = {
      create_access_key    = false,
      create_login_profile = false
    }
    }
  ]
}

# output "dev_iam_users_output" {
#   description = "Module AWS_IAM_Users outputs"
#   value       = module.dev_iam_user.*
#   # sensitive = true
# }

output "dev_iam_access_key" {
  description = "Module AWS_IAM_Users access key and secrets"
  value       = module.dev_iam_user.iam_access_key
  sensitive   = true
}

output "dev_iam_login_profile_encrypted_password" {
  description = "Module AWS_IAM_Users login password"
  value       = module.dev_iam_user.iam_login_profile_encrypted_password
  sensitive   = true
}
variable "groups" {
  type        = list(map(string))
  description = "List of IAM group names to be created with polices"
}

variable "users" {
  type = list(object({
    name    = string
    pgp_key = string
    groups  = list(string)
    permissions = object({
      create_access_key    = bool
      create_login_profile = bool
    })
  }))
  description = "List of IAM users to be created with groups and permissions"
}

variable "iam_user_tags" {
  type        = map(string)
  description = "Key-value map of tags for the IAM user. If configured with a provider default_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level."
  default     = {}
}
# AWS IAM Users

## Description

Provide AWS IAM users with configurable IAM group and IAM roles.


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Usage

```hcl
module "dev_iam_user" {
  source = "../.."

  groups = [{
    name = "test-admin",
    policy = jsonencode({
      "Version" : "2012-10-17"
      "Statement" : [
        {
          "Action" : [
            "ec2:Describe*",
          ],
          "Effect" : "Allow"
          "Resource" : "*"
        },
      ]
    })
    }
  ]

  users = [{
    name    = "john",
    pgp_key = "keybase:phatvo",
    groups  = ["test-admin"],
    permissions = {
      create_access_key    = true,
      create_login_profile = true
    }
    }, {
    name    = "alex",
    pgp_key = "keybase:phatvo",
    groups  = ["test-admin"],
    permissions = {
      create_access_key    = false,
      create_login_profile = false
    }
    }
  ]
}

```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_groups"></a> [groups](#input\_groups) | List of IAM group names to be created with polices | `list(map(string))` | n/a | yes |
| <a name="input_iam_user_tags"></a> [iam\_user\_tags](#input\_iam\_user\_tags) | Key-value map of tags for the IAM user. If configured with a provider default\_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level. | `map(string)` | `{}` | no |
| <a name="input_users"></a> [users](#input\_users) | List of IAM users to be created with groups and permissions | <pre>list(object({<br>    name    = string<br>    pgp_key = string<br>    groups  = list(string)<br>    permissions = object({<br>      create_access_key    = bool<br>      create_login_profile = bool<br>    })<br>  }))</pre> | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_iam_access_key"></a> [iam\_access\_key](#output\_iam\_access\_key) | IAM access keys and secrets |
| <a name="output_iam_group_arn"></a> [iam\_group\_arn](#output\_iam\_group\_arn) | The ARN assigned by AWS for the IAM group. |
| <a name="output_iam_group_id"></a> [iam\_group\_id](#output\_iam\_group\_id) | The IAM group's ID. |
| <a name="output_iam_group_path"></a> [iam\_group\_path](#output\_iam\_group\_path) | The IAM group's path in IAM. |
| <a name="output_iam_group_policy_id"></a> [iam\_group\_policy\_id](#output\_iam\_group\_policy\_id) | IAM group policy's ID |
| <a name="output_iam_group_unique_id"></a> [iam\_group\_unique\_id](#output\_iam\_group\_unique\_id) | The IAM group's unique\_id assgined by AWS. |
| <a name="output_iam_login_profile_encrypted_password"></a> [iam\_login\_profile\_encrypted\_password](#output\_iam\_login\_profile\_encrypted\_password) | IAM login encrypted\_password |
| <a name="output_iam_user_arn"></a> [iam\_user\_arn](#output\_iam\_user\_arn) | The ARN assigned by AWS for created users. |
| <a name="output_iam_user_tags_all"></a> [iam\_user\_tags\_all](#output\_iam\_user\_tags\_all) | A map of tags assigned to the resource, including those inherited from the provider default\_tags configuration block. |
| <a name="output_iam_user_unique_id"></a> [iam\_user\_unique\_id](#output\_iam\_user\_unique\_id) | The IAM user's unique\_id assigned by AWS. |  
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Run project with Terraform Commands

### I. Requirements

- Have terraform cli installed
- Have aws-vault installed, see guide [here](https://github.com/99designs/aws-vault#:~:text=AWS%20Vault%20is%20a%20tool,to%20your%20shell%20and%20applications.).
- Setup aws-vault to have aws account profile

### II. Command terraform commands

1. Init project

```bash
aws-vault exec keltio --no-session -- terraform init
```

2. Generate plan

```bash
aws-vault exec keltio --no-session -- terraform plan
```

3. Apply terraform plan

```bash
aws-vault exec keltio --no-session -- terraform apply --auto-approve
```

4. Destroy resources

```bash
aws-vault exec keltio --no-session -- terraform destroy --auto-approve
```

### III. Retrieve sensitve secrets

This section uses the basic example included in this module. So you may need to change the commands to suit your project.

#### 1. To retrieve a login password of a specific user

In the example, we used keybase to encrypt/decrypt login password

```bash
aws-vault exec keltio --no-session -- terraform output -json dev_iam_login_profile_encrypted_password |  jq -r '.<user_name>' | base64 --decode |  keybase pgp decrypt
```

#### 2. To retrieve IAM programming language credentials

```bash
aws-vault exec keltio --no-session -- terraform output -json dev_iam_access_key
```

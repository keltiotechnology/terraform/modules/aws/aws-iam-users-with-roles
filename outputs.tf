output "iam_user_arn" {
  description = "The ARN assigned by AWS for created users."
  value = {
    for k, iam_user in aws_iam_user.default : iam_user.name => iam_user.arn
  }
}

output "iam_user_tags_all" {
  description = "A map of tags assigned to the resource, including those inherited from the provider default_tags configuration block."
  value = {
    for k, iam_user in aws_iam_user.default : iam_user.name => iam_user.tags_all
  }
}

output "iam_user_unique_id" {
  description = "The IAM user's unique_id assigned by AWS."
  value = {
    for k, iam_user in aws_iam_user.default : iam_user.name => iam_user.unique_id
  }
}

output "iam_group_id" {
  description = "The IAM group's ID."
  value = {
    for k, iam_group in aws_iam_group.default : iam_group.name => iam_group.unique_id
  }
}

output "iam_group_arn" {
  description = "The ARN assigned by AWS for the IAM group."
  value = {
    for k, iam_group in aws_iam_group.default : iam_group.name => iam_group.arn
  }
}

output "iam_group_path" {
  description = "The IAM group's path in IAM."
  value = {
    for k, iam_group in aws_iam_group.default : iam_group.name => iam_group.path
  }
}

output "iam_group_unique_id" {
  description = "The IAM group's unique_id assgined by AWS."
  value = {
    for k, iam_group in aws_iam_group.default : iam_group.name => iam_group.unique_id
  }
}

output "iam_group_policy_id" {
  description = "IAM group policy's ID"
  value = {
    for k, iam_group_policy in aws_iam_group_policy.default : iam_group_policy.group => iam_group_policy.id
  }
}

output "iam_access_key" {
  description = "IAM access keys and secrets"
  value = {
    for k, access_key in aws_iam_access_key.default : access_key.user => {
      "access_key" : access_key.id,
      "secret_access_key" : access_key.secret
    }
  }

  sensitive = true
}

output "iam_login_profile_encrypted_password" {
  description = "IAM login encrypted_password"
  value = {
    for k, login_profile in aws_iam_user_login_profile.default : login_profile.user => login_profile.encrypted_password
  }
  sensitive = true
}
